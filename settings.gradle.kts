pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
rootProject.name = "currency-exchange"
include(":app", ":common")
includeBuild("DependenciesPlugin")
includeBuild("BuildConfigPlugin")
include( ":features:exchange",)

