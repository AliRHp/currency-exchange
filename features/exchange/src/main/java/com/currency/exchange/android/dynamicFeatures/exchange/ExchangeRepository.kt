package com.currency.exchange.android.dynamicFeatures.exchange

import androidx.lifecycle.LiveData
import com.currency.exchange.android.common.base.BaseRepositoryDelegation
import com.currency.exchange.android.common.exception.Failure
import com.currency.exchange.android.common.util.Either


interface ExchangeRepository {

    //region currency rate
    suspend fun getCurrencyRates(): Either<Failure, List<CurrencyRate>>
    fun getCurrencyRatesLive(): LiveData<List<CurrencyRate>>
    suspend fun getCurrencyRateWithName(currencyName: String): List<CurrencyRate>
    suspend fun saveCurrencyRates(rates: List<CurrencyRate>)
    //endregion

    //region balance
    fun getBalancesLive(): LiveData<List<Balance>>
    suspend fun getCurrencyBalanceWithName(currencyName: String): List<Balance>
    fun getInitBalanceChargeState(): Boolean
    fun saveInitBalanceChargeState()
    suspend fun saveBalances(balances: List<Balance>)
    //endregion
    fun getSuccessTradeCount(): Int
    fun saveSuccessTradCount(count: Int)
}

class ExchangeRepositoryImpl(
    private val network: ExchangeNetwork,
    private val balanceDao: BalanceDao,
    private val ratesDao: CurrencyRateDao,
    private val preferenceManager: ExchangeSharedPreferenceManager,
) : BaseRepositoryDelegation(), ExchangeRepository {

    override suspend fun getCurrencyRates(): Either<Failure, List<CurrencyRate>> {
        return networkRequest({ network.getRates() },
            { response ->
                response.rates?.toList()?.map {
                    CurrencyRate(it.first ?: "", it.second ?: 0.0)
                }?.toMutableList()
                    ?.apply {
                        add(
                            CurrencyRate(
                                response.base ?: "", 1.00
                            )
                        )
                    }
                    ?.toList() ?: listOf()
            }, CurrencyRateResponseDto.default())
    }

    override fun getBalancesLive(): LiveData<List<Balance>> {
        return balanceDao.liveLoadAllBalances()
    }

    override fun getCurrencyRatesLive(): LiveData<List<CurrencyRate>> {
        return ratesDao.liveLoadALlCurrencyRates()
    }

    override suspend fun getCurrencyRateWithName(currencyName: String): List<CurrencyRate> {
        return ratesDao.loadCurrencyRateWithName(currencyName)
    }

    override suspend fun getCurrencyBalanceWithName(currencyName: String): List<Balance> {
        return balanceDao.loadCurrencyBalanceWithName(currencyName)
    }

    override fun getInitBalanceChargeState(): Boolean {
        return preferenceManager.getInitBalanceChargeFlag()
    }

    override fun saveInitBalanceChargeState() {
        preferenceManager.saveInitBalanceChargeFlag()
    }

    override suspend fun saveBalances(balances: List<Balance>) {
        balanceDao.insertBalances(balances)
    }

    override suspend fun saveCurrencyRates(rates: List<CurrencyRate>) {
        ratesDao.insertRates(rates)
    }

    override fun getSuccessTradeCount(): Int {
        return preferenceManager.getTradeCount()
    }

    override fun saveSuccessTradCount(count: Int) {
        preferenceManager.saveTradeCount(count)
    }


}