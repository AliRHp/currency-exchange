package com.currency.exchange.android.dynamicFeatures.exchange.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.currency.exchange.android.common.base.BaseViewHolder
import com.currency.exchange.android.dynamicFeatures.exchange.Balance
import com.currency.exchange.android.dynamicFeatures.exchange.R
import kotlinx.android.synthetic.main.item_balance.view.*

class BalancesAdapter : RecyclerView.Adapter<BaseViewHolder<*>>() {
    override fun getItemCount(): Int { return data.size }
      var clickListener: (Balance) -> Unit = { _ -> }
    private val data: MutableList<Balance>
    init { data = ArrayList() }

    @SuppressLint("NotifyDataSetChanged")
    fun swapData(newData: List<Balance>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_balance, parent, false)
        return FestivalCategoryViewHolder(view, parent)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        try {
            val item = data[position]
            (holder as FestivalCategoryViewHolder).bind(item, clickListener = clickListener as (Any) -> Unit)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}


class FestivalCategoryViewHolder(val view: View, parent: ViewGroup) : BaseViewHolder<Balance>(view, parent) {
    override fun bind(item: Balance, clickListener: (Any) -> Unit ) {
        view.tvBalanceValue.text = (item.balanceValue ?: 0.0).toString()
        view.tvBalanceCurrency.text = item.currencyName ?: ""

    }


}

