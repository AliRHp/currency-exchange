package com.currency.exchange.android.dynamicFeatures.exchange.ui

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import com.currency.exchange.android.common.base.BaseFragment
import com.currency.exchange.android.common.extentions.setOnTextChangeListener
import com.currency.exchange.android.common.extentions.trimText
import com.currency.exchange.android.dynamicFeatures.exchange.*
import kotlinx.android.synthetic.main.fragment_convert.*


class ConvertFragment : BaseFragment<ExchangeViewModel>() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectLoginFeature()
    }
    override fun layoutId(): Int = R.layout.fragment_convert


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewComponents()

        setUpSelectCurrencies()

        viewModel.checkInitBalanceInsert()

        viewModel.startSyncRates()

        setUpSubmitTrade()

    }

    private  fun setupViewComponents(){
        val balancesAdapter = BalancesAdapter()
        rvBalances.adapter = balancesAdapter
        viewModel.getBalancesLive().observe(viewLifecycleOwner){
            balancesAdapter.swapData(it)
            viewModel.checkSellCurrency(it)
        }

        etSellAmount.setOnTextChangeListener {
            calculateConvertor()
        }
        viewModel.currencyConvertResult.observe(viewLifecycleOwner){receiveCurrencyAmount ->
            tvReceiveAmount.text = "+$receiveCurrencyAmount"
        }

    }

    private  fun setUpSelectCurrencies(){
        viewModel.sellCurrenciesResult.observe(viewLifecycleOwner){currenciesToSell ->
            linSelectSellCurrency.setOnClickListener {
                showSelectSellCurrencyDialog(currenciesToSell){
                    it?.currencyName?.let { selectedCurrencyName ->
                        tvSelectedSellCurrency.text = selectedCurrencyName
                        calculateConvertor()
                    }
                }
            }
        }

        viewModel.getCurrencyRatesLive().observe(viewLifecycleOwner){ currenciesToReceive ->
            calculateConvertor()
            linSelectReceiveCurrency.setOnClickListener {
                showSelectReceiveCurrencyDialog(currenciesToReceive){
                    it?.name?.let { selectedCurrencyName ->
                        tvSelectedReceiveCurrency.text = selectedCurrencyName
                        calculateConvertor()
                    }
                }
            }
        }

    }


    private fun setUpSubmitTrade() {
        btnSubmitTrade.setOnClickListener {
            viewModel.requestTrade(tvSelectedSellCurrency.trimText(),tvSelectedReceiveCurrency.trimText(),etSellAmount.trimText())
        }
        viewModel.tradeResult.observe(viewLifecycleOwner){
            if(it.success==true){
                showSuccessTradeDialog(it)
            }
            else {
                showErrorTradeDialog(it)
            }
        }
    }

    private fun calculateConvertor(){
        viewModel.currencyConvertor(tvSelectedSellCurrency.trimText(),tvSelectedReceiveCurrency.trimText(),etSellAmount.trimText())
    }

    private fun showSelectSellCurrencyDialog(currencies :List<Balance>, onSelectCurrency: (Balance?) -> Unit) {
         val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.choose_sell_currency_title))

        builder.setItems((currencies.map { it.currencyName }).toTypedArray()) { _,selectedIndex ->
            onSelectCurrency(currencies[selectedIndex])}
        val dialog = builder.create()
        dialog.show()
    }

    private fun showSelectReceiveCurrencyDialog(currencies :List<CurrencyRate>, onSelectCurrency: (CurrencyRate?) -> Unit) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.choose_receive_currency_title))

        builder.setItems((currencies.map { it.name }).toTypedArray()) { _,selectedIndex ->
            onSelectCurrency(currencies[selectedIndex])}
        val dialog = builder.create()
        dialog.show()
    }

    private  fun showSuccessTradeDialog(tradeResult: TradeResult){
        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.success_trade_title))
        builder.setMessage("You have converted ${tradeResult.sellAmount} ${tradeResult.sellCurrencyType}" +
                " to ${tradeResult.receiveAmount} ${tradeResult.receiveCurrencyType}" +
                " . commission Fee -${tradeResult.commissionFee} ${tradeResult.receiveCurrencyType}")

        builder.setPositiveButton(getString(R.string.done)) { _, _ -> }
        val dialog = builder.create()
        dialog.show()
    }

    private fun showErrorTradeDialog(tradeResult: TradeResult) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(getString(R.string.error_trade_title))
        var errorMessage = when(tradeResult.tradeError){
            TradeErrorTypes.NOT_ENOUGH_BALANCE -> resources.getString(com.currency.exchange.android.common.R.string.not_valid_referral_code)
            TradeErrorTypes.UNKNOWN -> resources.getString(R.string.unknown_trade_error)
            else -> resources.getString(R.string.unknown_trade_error)
        }
        builder.setPositiveButton(getString(R.string.Ok)) { _, _ ->

        }

        builder.setMessage(errorMessage)
        val dialog = builder.create()
        dialog.show()
     }



}

