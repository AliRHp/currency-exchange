package com.currency.exchange.android.dynamicFeatures.exchange

import retrofit2.Response
import retrofit2.http.GET


interface ExchangeNetwork {

    @GET("latest")
    suspend fun getRates(): Response<CurrencyRateResponseDto>


}