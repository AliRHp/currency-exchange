package com.currency.exchange.android.dynamicFeatures.exchange

import androidx.lifecycle.LiveData
import androidx.room.*


@Database(entities = [CurrencyRate::class ,Balance::class], version = 1, exportSchema = false)
abstract class ExchangeDataBase: RoomDatabase() {
    abstract val currencyRateDao: CurrencyRateDao
    abstract val balanceDao: BalanceDao

}
@Dao
interface CurrencyRateDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend  fun insertRates(rates: List<CurrencyRate>)

    @Query("select * FROM CurrencyRate")
    fun liveLoadALlCurrencyRates(): LiveData<List<CurrencyRate>>

    @Query("SELECT * FROM CurrencyRate WHERE name LIKE :currencyName")
    suspend fun loadCurrencyRateWithName(currencyName: String): List<CurrencyRate>



}

@Dao
interface BalanceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend  fun insertBalances(prices: List<Balance>)

    @Query("select * FROM Balance")
    fun liveLoadAllBalances(): LiveData<List<Balance>>

    @Query("SELECT * FROM Balance WHERE currencyName LIKE :currencyName")
    suspend fun loadCurrencyBalanceWithName(currencyName: String): List<Balance>


}





