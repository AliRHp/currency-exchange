package com.currency.exchange.android.dynamicFeatures.exchange.util


fun convertCurrency(sourceRate:Double, destinationRate:Double,amount:Double):Double{
    val result =  destinationRate / sourceRate
    return (result * amount)
}


