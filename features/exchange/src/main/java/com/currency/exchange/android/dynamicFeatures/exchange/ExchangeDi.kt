package com.currency.exchange.android.dynamicFeatures.exchange

import com.currency.exchange.android.common.di.*
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.qualifier.named
import org.koin.dsl.module


fun injectLoginFeature() = loadExchangeModule
private val loadExchangeModule by lazy {
    loadKoinModules(exchangeModule)
}

val exchangeModule= module {

    //config data base di
    single(named(EXCHANGE_DATA_BASE_NAME)) { "exchanges.db" }
    single(named(EXCHANGE_DATA_BASE)) { getDatabase(get(), ExchangeDataBase::class.java, get(named(EXCHANGE_DATA_BASE_NAME))) }
    single(named(CURRENCY_RATE_DAO)) { (get(named(EXCHANGE_DATA_BASE)) as ExchangeDataBase).currencyRateDao }
    single(named(BALANCES_DAO)) { (get(named(EXCHANGE_DATA_BASE)) as ExchangeDataBase).balanceDao }



    //config preferences di
    single(named(EXCHANGE_PREFERENCES_MANAGER)) { GeneralBaseInfoPreferenceManagerImp(get(named(APP_PREFERENCES))) }


    //config repository di
    single<ExchangeRepository> { ExchangeRepositoryImpl(createNetwork(ExchangeNetwork::class.java
        ,get(named(AUTHENTICATED_RETROFIT))),
        get(named(BALANCES_DAO)),
        get(named(CURRENCY_RATE_DAO)),
        get(named(EXCHANGE_PREFERENCES_MANAGER))) }

    //config viewModel di
    viewModel { ExchangeViewModel(get()) }
}