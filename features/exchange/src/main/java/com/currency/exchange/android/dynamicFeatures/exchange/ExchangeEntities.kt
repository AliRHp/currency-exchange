package com.currency.exchange.android.dynamicFeatures.exchange

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.currency.exchange.android.common.base.BaseResponseModel
import com.currency.exchange.android.common.exception.Failure


//region CONSTANTS
const val EXCHANGE_DATA_BASE_NAME = "EXCHANGE_DATA_BASE_NAME"
const val EXCHANGE_DATA_BASE = "EXCHANGE_DATA_BASE"
const val BALANCES_DAO = "BALANCES_DAO"
const val CURRENCY_RATE_DAO = "CURRENCY_RATE_DAO"
const val EXCHANGE_PREFERENCES_MANAGER = "EXCHANGE_PREFERENCES_MANAGER"


const val INIT_BALANCE_CHARGE_PREF_KEY = "INIT_BALANCE_CHARGE_PREF_KEY"
const val SUCCESS_TRADE_PREF_KEY = "SUCCESS_TRADE_PREF_KEY"

const val defaultEURCurrencyName = "EUR"
const val defaultUSDCurrencyName = "USD"
const val defaultBGNCurrencyName = "BGN"
const val defaultALlCurrencyName = "All"
val initBalances =
    listOf(
        Balance(defaultEURCurrencyName, 1000.00)
        , Balance(defaultUSDCurrencyName, 0.00)
        , Balance(defaultBGNCurrencyName, 0.00)
    )


const val SYNC_RATE_PERIOD_MILLI_SECONDS = 5 * 1000L


val initCommissionRules =
    listOf(
        CommissionRule(0, 0.70, defaultALlCurrencyName),
        CommissionRule(10, 0.03, defaultALlCurrencyName),
        CommissionRule(200, 0.00, defaultEURCurrencyName)
    )


//endregion


//region network requests

data class CurrencyRateResponseDto(val rates: Map<String?, Double?>?, val base: String?) :
    BaseResponseModel() {
    companion object {
        fun default(): CurrencyRateResponseDto = CurrencyRateResponseDto(mapOf(), defaultEURCurrencyName)
    }
}
//endregion


// region entities
@Entity
data class Balance(@PrimaryKey val currencyName: String, val balanceValue: Double?)


@Entity
data class CurrencyRate(@PrimaryKey val name: String, val rate: Double?)


data class CommissionRule(val count: Int, val commissionFee: Double?, val currencyName: String)


data class TradeResult(
    val success: Boolean?,
    val tradeError: TradeErrorTypes?,
    val sellAmount: Double? = null,
    val sellCurrencyType: String? = null,
    val receiveAmount: Double? = null,
    val receiveCurrencyType: String? = null,
    val commissionFee: Double? = null
)

enum class TradeErrorTypes {
    NOT_ENOUGH_BALANCE,
    UNKNOWN,

}
//endregion


