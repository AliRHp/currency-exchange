package com.currency.exchange.android.dynamicFeatures.exchange

import android.content.SharedPreferences
import com.currency.exchange.android.common.extentions.setPref


interface ExchangeSharedPreferenceManager {
    fun saveInitBalanceChargeFlag()
    fun saveTradeCount(count:Int)
    fun getTradeCount():Int
    fun getInitBalanceChargeFlag():Boolean
}

class GeneralBaseInfoPreferenceManagerImp(private val  prefs: SharedPreferences): ExchangeSharedPreferenceManager {

    override fun saveInitBalanceChargeFlag( ) {
        prefs.setPref(INIT_BALANCE_CHARGE_PREF_KEY,true)
    }

    override fun saveTradeCount(count: Int) {
        prefs.setPref(SUCCESS_TRADE_PREF_KEY,count)
     }

    override fun getTradeCount(): Int {
        return  prefs.getInt(SUCCESS_TRADE_PREF_KEY,0)
    }

    override fun getInitBalanceChargeFlag() :Boolean{
      return  prefs.getBoolean(INIT_BALANCE_CHARGE_PREF_KEY,false)
    }

}