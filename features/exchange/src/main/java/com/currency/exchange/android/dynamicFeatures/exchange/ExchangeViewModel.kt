package com.currency.exchange.android.dynamicFeatures.exchange

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.currency.exchange.android.common.base.BaseViewModel
import com.currency.exchange.android.common.util.livedata.SingleLiveEvent
import com.currency.exchange.android.dynamicFeatures.exchange.util.convertCurrency
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.NumberFormatException

class ExchangeViewModel(private val repository: ExchangeRepository) : BaseViewModel() {


    fun checkInitBalanceInsert() {
        if (!repository.getInitBalanceChargeState()) {
            viewModelScope.launch {
                repository.saveBalances(initBalances)
                repository.saveInitBalanceChargeState()
            }
        }
    }

    fun getBalancesLive() = Transformations.map(repository.getBalancesLive()) { balance ->
        balance.sortedBy { it.balanceValue }.reversed()
    }

    fun getCurrencyRatesLive() = repository.getCurrencyRatesLive()

    fun startSyncRates() {
        viewModelScope.launch {
            while (true) {
                repository.getCurrencyRates().either({ handleFailure(it) }) {
                    viewModelScope.launch {
                        repository.saveCurrencyRates(it)
                    }
                }
                delay(SYNC_RATE_PERIOD_MILLI_SECONDS)

            }
        }
    }


    private val _sellCurrenciesResult: MutableLiveData<List<Balance>> = MutableLiveData()
    val sellCurrenciesResult: LiveData<List<Balance>>
        get() = _sellCurrenciesResult
    fun checkSellCurrency(balances: List<Balance>) {
        _sellCurrenciesResult.postValue(balances.filter { it.balanceValue != null && it.balanceValue > 0 })
    }

    private val _currencyConvertResult: MutableLiveData<Double> = SingleLiveEvent()
    val currencyConvertResult: LiveData<Double> = _currencyConvertResult
    fun currencyConvertor(
        sellCurrencyRateName: String,
        receiveCurrencyRateName: String,
        sellAmountInString: String
    ) {
        if (!validateTradeParameters(
                sellCurrencyRateName,
                receiveCurrencyRateName,
                sellAmountInString
            )
        ) {
            _currencyConvertResult.postValue(0.00)
            return
        }

        viewModelScope.launch(IO) {
            val sellCurrencyRates = repository.getCurrencyRateWithName(sellCurrencyRateName)
            val receiveCurrencyRates = repository.getCurrencyRateWithName(receiveCurrencyRateName)
            if (sellCurrencyRates.isEmpty() || receiveCurrencyRates.isEmpty()) return@launch

            val sellCurrencyRate = sellCurrencyRates[0]
            val receiveCurrencyRate = receiveCurrencyRates[0]

            if (sellCurrencyRate.rate != null &&
                receiveCurrencyRate.rate != null
            ) {
                _currencyConvertResult.postValue(
                    convertCurrency(
                        sellCurrencyRate.rate,
                        receiveCurrencyRate.rate,
                        amount = sellAmountInString.toDouble()
                    )
                )
            }

        }
    }


    private val _tradeResult: MutableLiveData<TradeResult> = MutableLiveData()
    val tradeResult: LiveData<TradeResult>
        get() = _tradeResult
    fun requestTrade(
        sellCurrencyRateName: String,
        receiveCurrencyRateName: String,
        sellAmountInString: String
    ) {

        //check parameters validation before start trade
        if (!validateTradeParameters(
                sellCurrencyRateName,
                receiveCurrencyRateName,
                sellAmountInString
            )
        ) {
            _currencyConvertResult.postValue(0.00)
            return
        }

        val sellAmount = sellAmountInString.toDouble()

        viewModelScope.launch(IO) {
            val sellCurrencyRates = repository.getCurrencyRateWithName(sellCurrencyRateName)
            val receiveCurrencyRates = repository.getCurrencyRateWithName(receiveCurrencyRateName)
            if (sellCurrencyRates.isEmpty() || receiveCurrencyRates.isEmpty()) return@launch

            val sellCurrencyRate = sellCurrencyRates[0]
            val receiveCurrencyRate = receiveCurrencyRates[0]

            var receiveAmount = 0.0
            if (sellCurrencyRate.rate != null &&
                receiveCurrencyRate.rate != null
            ) {
                receiveAmount = (convertCurrency(
                    sellCurrencyRate.rate,
                    receiveCurrencyRate.rate,
                    amount = sellAmount
                ))
            }

            if (receiveAmount < 0) {
                return@launch
            }

            val balanceInSellCurrencies =
                repository.getCurrencyBalanceWithName(sellCurrencyRateName)
            if (balanceInSellCurrencies.isEmpty()) {
                _tradeResult.postValue(TradeResult(false, TradeErrorTypes.UNKNOWN))
                return@launch
            }


            //if  the balance not enough for amount and commission send error to view
            val balanceInSellCurrency = balanceInSellCurrencies[0]
            val commissionFee = calculateCommission(sellCurrencyRateName)
            if (balanceInSellCurrency.balanceValue ?: 0.0 < sellAmount + commissionFee) {
                _tradeResult.postValue(TradeResult(false, TradeErrorTypes.UNKNOWN))
                return@launch
            }


            // start trade transaction
            var receiveCurrentBalance = 0.0
            val balanceInReceiveCurrencies =
                repository.getCurrencyBalanceWithName(receiveCurrencyRateName)
            if (balanceInReceiveCurrencies.isNotEmpty()) receiveCurrentBalance = balanceInReceiveCurrencies[0].balanceValue ?: 0.0

            val finalSellBalance = balanceInSellCurrency.balanceValue!! - sellAmount - commissionFee
            val finalReceiveBalance = receiveCurrentBalance + receiveAmount


            //save trade count and new balances
            repository.saveBalances(
                listOf(
                    Balance(receiveCurrencyRateName, finalReceiveBalance),
                    Balance(sellCurrencyRateName, finalSellBalance)
                )
            )
            repository.saveSuccessTradCount(repository.getSuccessTradeCount() + 1)

            _tradeResult.postValue(
                TradeResult(
                    true,
                    null,
                    sellAmount,
                    sellCurrencyRateName,
                    receiveAmount,
                    receiveCurrencyRateName,
                    commissionFee
                )
            )


        }
    }


    /*calculate commission fee based on dynamic commission rules */
    private fun calculateCommission(sellCurrencyRateName: String): Double {
        val commissionRules = initCommissionRules
        val successTradesCount = repository.getSuccessTradeCount()
        var finalCommissionFeeRate = 0.0
        commissionRules.forEach {
            if (successTradesCount >= it.count && (it.currencyName == "All" || it.currencyName == sellCurrencyRateName)) {
                finalCommissionFeeRate = it.commissionFee ?: 0.0
            }
        }
        return finalCommissionFeeRate
    }


    private fun validateTradeParameters(
        sellCurrencyRateName: String,
        receiveCurrencyRateName: String,
        sellAmountInString: String
    ): Boolean {
        try {
            if (sellAmountInString.toDouble() <= 0) {
                return false
            }
        } catch (e: NumberFormatException) {
            return false
        }
        if (sellCurrencyRateName.length < 3) return false
        if (receiveCurrencyRateName.length < 3) return false

        return true

    }

}