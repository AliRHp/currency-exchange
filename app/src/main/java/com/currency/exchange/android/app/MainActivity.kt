package com.currency.exchange.android.app

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // without any navigation go to single feature in application
        loadExchangeModule()

    }



    /* load exchange module in reflection way  */
    private fun loadExchangeModule(){
        //todo move this part to dynamic navigation
        val activityToStart = "com.currency.exchange.android.dynamicFeatures.exchange.ui.ExchangeActivity"
        try {
            val c = Class.forName(activityToStart)
            val intent = Intent(this, c)
            startActivity(intent)
        } catch (ignored: ClassNotFoundException) {
        }
    }
}

