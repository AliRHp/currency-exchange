package com.currency.exchange.android.app

import com.currency.exchange.android.common.di.*
import org.koin.core.qualifier.named
import org.koin.dsl.module



val appModule = module {

    single(named(APP_BASE_URL)) { "http://api.exchangeratesapi.io/v1/" }

    single(named(APP_NETWORK_TIMEOUT)) { 30000 }

    single(named(APP_PREFERENCES_NAME)) {  "PREFRENCES"}

}



