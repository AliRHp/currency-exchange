package com.currency.exchange.android.app

import android.app.Application
import com.currency.exchange.android.common.di.assetModule
import com.currency.exchange.android.common.di.networkModule
import com.currency.exchange.android.common.di.preferenceModule
import com.currency.exchange.android.common.di.resourcesModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CurrencyExchangeApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(listOf(
                appModule,networkModule, preferenceModule,
                assetModule, resourcesModule
            ))
            androidContext(applicationContext)
        }
    }
}