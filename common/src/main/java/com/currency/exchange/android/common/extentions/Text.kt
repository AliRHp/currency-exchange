package com.currency.exchange.android.common.extentions

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.TextView
import java.text.DecimalFormat


fun EditText.trimText() = this.text.toString().trim()


fun TextView.trimText() = this.text.toString().trim()


fun EditText.setOnTextChangeListener(onChange: (text: String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {


        override fun afterTextChanged(s: Editable?) {
            onChange(trimText())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

    })

}