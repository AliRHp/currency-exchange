package com.currency.exchange.android.common.util.mockserver

import com.currency.exchange.android.common.BuildConfig
import okhttp3.Interceptor
import okhttp3.Protocol
import okhttp3.Response
import okhttp3.ResponseBody
import java.io.IOException

class MockInterceptor : Interceptor {


    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var response: Response? = null
        var fakeResponse = ""
        if (BuildConfig.DEBUG) {
            try {
                Thread.sleep(1500)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

            fakeResponse =
                    when {
                        chain.request().url.toString().contains("latest") -> json
                        else -> ""
                    }

            response = Response.Builder()
                    .code(200)
                    .message(fakeResponse)
                    .request(chain.request())
                    .protocol(Protocol.HTTP_1_0)
                    .body(ResponseBody.create(null, fakeResponse.toByteArray()))
                    .addHeader("content-type", "application/json")
                    .build()
        } else {
            response = chain.proceed(chain.request())
        }
        return response
    }


    val json = """{
    "success": true,
    "timestamp": 1519296206,
    "base": "EUR",
    "date": "2021-03-17",
    "rates": {
        "AUD": 1.566015,
        "CAD": 1.560132,
        "CHF": 1.154727,
        "CNY": 7.827874,
        "GBP": 0.882047,
        "JPY": 132.360679,
        "USD": 1.103
    }
} """.trim()

}

