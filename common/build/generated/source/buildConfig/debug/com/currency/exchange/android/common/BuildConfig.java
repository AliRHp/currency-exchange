/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.currency.exchange.android.common;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.currency.exchange.android.common";
  public static final String BUILD_TYPE = "debug";
}
